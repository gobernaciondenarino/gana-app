<?php
	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Funcion para ordena los datos Seccion del funcionario
	* Cambios: none */
 	function ordenarDatosSeccion($a, $b) {
    	if ($a['orden_seccion'] == $b['orden_seccion']) {
	        return 0;
	    }
	    return $a['orden_seccion'] < $b['orden_seccion'] ? -1 : 1;
	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Funcion para ordenarlos datos de los campos del funcionario
	* Cambios: none */
	function ordenarDatosCampos($a, $b) {
	    if ($a['orden'] == $b['orden']) {
        	return 0;
    	}
    	return $a['orden'] < $b['orden'] ? -1 : 1;
	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Funcion para ordenarlos datos de los campos del funcionario
	* Cambios: none */
	function ordenarConsolidados($a, $b) {
	    if ($a['anio'] == $b['anio']) {
        	return 0;
    	}
    	return $a['anio'] > $b['anio'] ? -1 : 1;
	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Consultar los funcionarios
	* Cambios: none */
 	function consultarFuncionarios(){
		$response = @file_get_contents('http://aplicaciones.narino.gov.co/api/assets/v1/ConsultarListaDirectivos');
		if($response === FALSE){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente");
		}
	  	$response = json_decode($response, true);
	  	return $response;
 	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Consultar los consolidados
	* Cambios: none */
  	function consultarConsolidados(){
	  	$response = @file_get_contents('http://aplicaciones.narino.gov.co/api/assets/v1/ConsultarConsolidados');
		if($response === FALSE){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente");
		}
	  	$response = json_decode($response, true);
		usort($response["lst_consolidados"], 'ordenarConsolidados');
	  	return $response["lst_consolidados"];
 	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 14/03/2017
	* Descripción: Consultar la informacion detallada de un funcionario
	* Cambios: none */
 	function consultarInformacionFuncionario($pNroId){
 		$response = @file_get_contents('http://aplicaciones.narino.gov.co/api/assets/v1/ConsultarInformacionFuncionario/'.$pNroId);
		if($response === FALSE){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente");
		}
	  	$response = json_decode($response, true); 		  
	  	$resumenFuncionario = "";
	  	//TODO Modificar cuando se tenga los servicios
		$registroEncontrado = false;  		
	  	if(count($response) > 0){
			foreach($response as $funcionario=>$value){
				if($value['nro_id'] == $pNroId){					
					$resumenFuncionario = $value;
					usort($resumenFuncionario['secciones'], 'ordenarDatosSeccion');				
					//Se realiza foreach para ordenar las listas de los campos
					foreach($resumenFuncionario['secciones'] as $campo=>$valueCampo){	
						if(count($resumenFuncionario['secciones'][$campo]['lst_campos']) > 0){			
							usort($resumenFuncionario['secciones'][$campo]['lst_campos'], 'ordenarDatosCampos');
						}
					}
					$registroEncontrado = true;	
					break;
				}				
			}			
	  	}
		if(!$registroEncontrado){
			throw new Exception("Oops!! No se ha encontrado información para el registro seleccionado.");
		}  	
	  	return $resumenFuncionario;
 	}
?>