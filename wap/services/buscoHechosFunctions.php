<?php
	/*
	* Autor: Manuel Obando
	* Fecha: 15/03/2017
	* Descripción: Consultar el listado de informes dependiendo del parametro
	* Cambios: none */
	function consultarListadoInformes($pTexto){
    	$response = @file_get_contents('http://aplicaciones.narino.gov.co/api/reports/v1/ObtenerListaInformes/texto/'.$pTexto);	
		if($response === FALSE){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente");
		}
		$response = json_decode($response, true);
	 	return $response;     
 	}

	/*
	* Autor: Manuel Obando
	* Fecha: 15/03/2017
	* Descripción: Consultar el detalle del informe
	* Cambios: none */
 	function consultarInformacionInforme($pIdInforme){
     	$response = file_get_contents('http://aplicaciones.narino.gov.co/api/reports/v1/ObtenerDetalleInforme/'.$pIdInforme);
		 if($response === FALSE){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente");
		}
	 	$response = json_decode($response, true);
	 	return $response;     
 	} 
?>