<?php
	/*
	* Autor: Anderson Benavides
	* Fecha: 13/03/2017
	* Descripción: Consultar los temas creados para gana datos
	* Cambios: none */
 	function consultarTemas(){
		$response = @file_get_contents('https://datos.narino.gov.co/?q=api/3/gana/term_list&type=3');
	  	$response = json_decode($response, true);
		if(!$response["success"]){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente.");
		}
	  	return $response["result"];
 	}

	/*
	* Autor: Anderson Benavides
	* Fecha: 13/03/2017
	* Descripción: Consultar las etiquetas creadas para gana datos
	* Cambios: none */
 	function consultarEtiquetas(){
	  	$response = @file_get_contents('https://datos.narino.gov.co/?q=api/3/gana/term_list&type=2');
	  	$response = json_decode($response, true);
		if(!$response["success"]){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente.");
		}
	  	return $response["result"];
 	}

 	/*
	* Autor: Anderson Benavides
	* Fecha: 13/03/2017
	* Descripción: Consultar los datasets que hacen referencia a un tema o etiqueta
	* Cambios: none */
 	function consultarDatasets($pUuid){
 	  	$response = @file_get_contents('https://datos.narino.gov.co/?q=api/3/gana/dataset_by_term&id='.$pUuid);
	  	$response = json_decode($response, true);
		if(!$response["success"]){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente.");
		}	  	
	  	return $response["result"];
 	}

 	/*
	* Autor: Anderson Benavides
	* Fecha: 13/03/2017
	* Descripción: Consultar los recursos que tiene un dataset
	* Cambios: none */
 	function consultarRecursos($pId){
 	  	$response = @file_get_contents('https://datos.narino.gov.co/?q=api/3/action/package_show&id='.$pId);
	  	$response = json_decode($response, true);
		if(!$response["success"]){
			throw new Exception("Oops!! Lo sentimos, algo ha salido mal. Intenta nuevamente.");
		} 	  	
	  	return $response["result"][0]["resources"];
 	}
?>