<?php
  header("Content-type: text/vnd.wap.wml");
  header('Content-Type: text/html; charset=utf-8');
  include_once('../../services/ganaDatosFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">

<wml>
    <card id="RecursosCards">
        <p id="logoGana" align='center'>
            <img src="../../img/Logo_GANA_DATOS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <p align="center">
            <b>Recursos Disponibles en el Portal GANA</b><br/>
        </p>
        <p>
        <?php  
        try{
            $registros = consultarRecursos($_GET["id"]);
            if(count($registros) > 0){
                foreach ($registros as $recurso=>$val) {
                    $divStyle = ($recurso%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
                    echo $divStyle."<a href=".$val['url']." style='text-decoration:none;'>".$val['name'].'<br/></a>';
                    $existeSalto = strpos($val['description'], '<p>');
                    if ($existeSalto === false) {
                        echo $val['description'];
                    } else {
                        echo substr($val['description'], $existeSalto + 3);
                    } 
                    echo "</div>";               
                }
            }else{
                echo "No existen registros para los criterios de búsqueda.";
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
  	    ?>
        </p>
        <p align='center'><small>Visita http://gana.nariño.gov.co para acceder a más información</small></p>
        <p id="logoGobernacion" align='center'>    
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>