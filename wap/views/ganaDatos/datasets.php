<?php
  header("Content-type: text/vnd.wap.wml");
  header('Content-Type: text/html; charset=utf-8');
  include_once('../../services/ganaDatosFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">

<wml>  
  <card id="DatasetsCards">
    <p id="logoGana" align='center'>
      <img src="../../img/Logo_GANA_DATOS.gif"/><br/>
    </p> 
    <p id="logoHome">
      <img src="../../img/home.gif"/>
      <a href='../../index.php'>Home Gana</a>
    </p> 
    <p align="center">
      <b>Datasets Portal GANA</b><br/>
    </p>
    <p>
    <?php  
    try{
      $registros = consultarDatasets($_GET["id"]);
      if(count($registros) > 0){
        foreach ($registros as $dataset=>$val) {
          $divStyle = ($dataset%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
          echo $divStyle."<a href='recursos.php?id=".$val['id']."' style='text-decoration:none;'>".$val['name'].'<br/></a>';
          echo "</div>";           
        }
      }else{
        echo "No existen registros para los criterios de búsqueda.";
      }
    }catch(Exception $e){
        echo $e->getMessage();
    }
    ?>
    </p>
    <p id="logoGobernacion" align='center'>
      <img src="../../img/Logo Gobernacion.gif"/>
    </p>
  </card>
</wml>