<?php
 header("Content-type: text/vnd.wap.wml");  
 header('Content-Type: text/html; charset=utf-8');
 include_once('../../services/ganaDatosFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">
<wml>
 <card id="EtiquetasCard"> 
    <p id="logoGana" align='center'>
        <img src="../../img/Logo_GANA_DATOS.gif"/>
        <br/>
    </p> 
    <p id="logoHome">
        <img src="../../img/home.gif"/>
        <a href='../../index.php'>Home Gana</a>
    </p> 
    <p align="center">
    <b>Etiquetas del Portal de Datos Abiertos</b>
    <br/>
    </p>
    <p>
    <?php  
    try{
	   foreach (consultarEtiquetas() as $etiqueta=>$val) {
           $divStyle = ($etiqueta%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
           echo $divStyle."<a href='datasets.php?id=".$val['uuid']."' style='text-decoration:none;'>".$val['name'].'<br/></a></div>';
	   	}
    }catch(Exception $e){
        echo $e->getMessage();
    }
  	?>       
    </p>
    <p id="logoGobernacion" align='center'>
        <img src="../../img/Logo Gobernacion.gif"/>
    </p>
 </card>
</wml>