<?php
  header("Content-type: text/vnd.wap.wml");
  header('Content-Type: text/html; charset=utf-8');
  include_once('../../services/declaracionesJuramentadasFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">

<wml>  
    <card id="ResumenFuncionarioCards">    
        <p id="logoGana" align='center'>
            <img src="../../img/Logo_DECLARACIONES_JURAMENTADAS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <?php
        try{
            $registros = consultarInformacionFuncionario($_GET["nroId"]);   
        ?>
        <p align='center'>
        <?php           
            echo "<b>".$registros['primer_nombre']." ".$registros['segundo_nombre']." ".$registros['primer_apellido']." ".$registros['segundo_apellido']."</b><br/>";
            echo $registros['profesion'] != "" ? $registros['profesion']."<br/>" : "";
            echo $registros['dependencia'] != "" ? "<small>".$registros['dependencia']."</small><br/>" : "";
            echo $registros['sub_dependencia'] != "" ? "<small>".$registros['sub_dependencia']."</small><br/>" : "";
            echo $registros['email'] != "" ? $registros['email'] : "";
            echo "<br/><br/><a href=".$registros['url_acta_posesion'].">Acta de Posesión ".date ('Y')."</a>";
  	    ?>
        </p>
        <p>
        <?php
            foreach ($registros["secciones"] as $seccion=>$val) {
                echo "<fieldset style='background-color: #e5e7e3;'>";
                echo "<font color='#50961F'>".$val["nombre_seccion"]."</font><br/>";
                if(count($val["lst_campos"]) > 0){
                    foreach($val["lst_campos"] as $detalles=>$valDetalle){
                        if($valDetalle["tipo_campo"] == 'money'){
                            echo "<b>".$valDetalle["label_campo"]."</b>: $".number_format($valDetalle["valor_campo"])."<br/>";
                        }else if($valDetalle["tipo_campo"] == 'date'){
                            $fechaMod = explode("-", $valDetalle["valor_campo"]);
                            echo "<b>".$valDetalle["label_campo"]."</b>: ".$fechaMod[1]."-".$fechaMod[2]."-".$fechaMod[0]."<br/>";
                        }else{
                            $descarga = ($valDetalle["url_campo"] == "" || $valDetalle["url_campo"] == null) ? $valDetalle["valor_campo"] : "<a href=".$valDetalle['url_campo'].">Ver</a>";
                            echo "<b>".$valDetalle["label_campo"]."</b>: ".$descarga."<br/>";
                        }
                    }
                }else{
                    echo "No se encontraron registros";
                }
                echo "</fieldset><br/>";
            }
        ?>
        </p>
        <?php 
        }catch(Exception $e){
            echo '<p align="center">'.$e->getMessage().'</p>' ;
        }
        ?>
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>