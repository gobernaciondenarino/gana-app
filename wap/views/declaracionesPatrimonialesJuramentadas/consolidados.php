<?php
 header("Content-type: text/vnd.wap.wml");  
 header('Content-Type: text/html; charset=utf-8');
 include_once('../../services/declaracionesJuramentadasFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">
<wml>
    <card id="ConsolidadosCard"> 
        <p id="logoGana" align='center'>
            <img src="../../img/Logo_DECLARACIONES_JURAMENTADAS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <p align="center">
            <b>Consolidados Declaraciones Patrimoniales Juramentadas</b><br/>
        </p>
        <p>
        <?php  
        try{
            $registros = consultarConsolidados();
            if(count($registros) > 0){
	            foreach ($registros as $consolidado=>$val) {
                    $divStyle = ($consolidado%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
                    echo $divStyle."<a href=".$val['url'].">".$val['anio'].'<br/></a></div>';
	   	        }
            }else{
                echo "No existen registros para los criterios de búsqueda.";
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
  	    ?>       
        </p>
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>