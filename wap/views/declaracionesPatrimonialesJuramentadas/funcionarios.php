<?php
 header("Content-type: text/vnd.wap.wml");  
 header('Content-Type: text/html; charset=utf-8');
 include_once('../../services/declaracionesJuramentadasFunctions.php');
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">
<wml>
    <card id="FuncionariosCard">
        <p id="logoGana" align='center'>
            <img src="../../img/Logo_DECLARACIONES_JURAMENTADAS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <p align="center">
            <b>Funcionarios de Nivel Directivo de la Gobernación de Nariño</b><br/>
        </p>
        <p>
        <?php  
            try{
                $registros = consultarFuncionarios();
                if(count($registros) > 0){
                    foreach ($registros as $funcionario=>$val) {
                        $divStyle = ($funcionario%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
                        echo $divStyle."<a href='declaracionPatrimonialFuncionario.php?nroId=".$val['nro_id']."'>".$val['primer_nombre'].' '.$val['segundo_nombre'].' '.$val['primer_apellido'].' '.$val['segundo_apellido'].'</a><br/>';
                        echo "<small>".$val['cargo'].'</small><br/>';
                        echo "<small><small>".$val['dependencia'].'</small></small><br/>';
                        echo $val['sub_dependencia'] != "" ? "<small><small>".$val['sub_dependencia'].'</small></small></div><br/>' : "</div><br/>";
                    }
                }else{
                    echo "No existen registros para los criterios de búsqueda.";
                }	  
            }catch(Exception $e){
                echo $e->getMessage();
            }
  	    ?>       
        </p>
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>