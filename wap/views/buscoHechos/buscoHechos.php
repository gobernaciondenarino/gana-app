<?php 
 header("Content-type: text/vnd.wap.wml");
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN" "http://www.wapforum.org/DTD/wml12.dtd">
<wml>
    <card id="buscoHechosCard">
        <p id="logoBuscoHechos" align='center'>
            <img src="../../img/Logo_BUSCO_HECHOS.gif" alt="Busco Hechos"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <p align="center">
            <b>Informes de Gestión de los Funcionarios de la Gobernación de Nariño</b><br/><br/>
        </p>
        <p align="center">
            Estoy interesado en: <input type="text" id="interesado" name="interesado" maxlength="25" emptyok="false" value="" />
            <?php 
                if(isset($_GET['mensaje'])){
                    echo "<br/><i>Debes ingresar un texto para realizar la búsqueda.</i><br/>";
                }
            ?>
            <br/>
            <anchor>
                Buscar
                <go href="informes.php" method="get">
                    <postfield name="interesado" value="$(interesado)"/>
                </go>
            </anchor>
        </p>
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>