<?php
 header("Content-type: text/vnd.wap.wml");  
 header('Content-Type: text/html; charset=utf-8');
 include_once('../../services/buscoHechosFunctions.php');
     
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">
<wml>
    <card id="buscoHechosCard"> 
        <p id="logoBuscoHechos" align='center'>
            <img src="../../img/Logo_BUSCO_HECHOS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p>
        <?php 
        try{
            $registro = consultarInformacionInforme($_GET["idInforme"]);
        ?>
        <p align="center">
            <b><?php  echo $registro[0]['titulo_informe']?></b><br/>
            <small><?php  echo $registro[0]['fecha_informe']?></small>
        </p>
        <?php
            if(count($registro) > 0){   
                echo "<fieldset style='background-color: #e5e7e3;'>";
                echo "<font>".$registro[0]["texto_completo_informe"]."</font>";            
                echo "</fieldset>";    
                echo "<p align='center'><a href=".$registro[0]['url_informe'].">Descargar Informe Completo</a><p>";                 
            }else{
                echo "<fieldset style='background-color: #e5e7e3;'>";
                echo "No existen registros para los criterios de búsqueda.";
                echo "</fieldset><br/>";  
            }
        }catch(Exception $e){
            echo '<p align="center">'.$e->getMessage().'</p>' ;
        }        
  	    ?> 
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>