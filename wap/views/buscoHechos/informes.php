<?php
 header("Content-type: text/vnd.wap.wml");  
 header('Content-Type: text/html; charset=utf-8');
 include_once('../../services/buscoHechosFunctions.php');
 $interesado = trim($_GET["interesado"]);
 if($interesado == 'null' || $interesado == ''){
     header('Location: buscoHechos.php?mensaje=1');
     die();
 }
?>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.2//EN"
"http://www.wapforum.org/DTD/wml12.dtd">
<wml>
    <card id="buscoHechosCard">
        <p id="logoBuscoHechos" align='center'>
            <img src="../../img/Logo_BUSCO_HECHOS.gif"/><br/>
        </p> 
        <p id="logoHome">
            <img src="../../img/home.gif"/>
            <a href='../../index.php'>Home Gana</a>
        </p> 
        <p align="center">
            <b>Informes de Gestión de los Funcionarios de la Gobernación de Nariño</b><br/>
        </p>
        <p>
        <?php  
        try{
            $registros = consultarListadoInformes($interesado);
            if(count($registros) > 0){
                foreach ($registros as $informe=>$val) {
                    $divStyle = ($informe%2==0) ? "<div style='background-color:#e5e7e3;'>" : "<div>";
                    echo $divStyle."<a href='detalleInforme.php?idInforme=".$val['id_informe']."'>".$val['titulo_informe'].'<br/></a>';
                    echo "<small>".$val['fecha_informe']."</small><br/><br/>";
                    echo "</div>";
                }
            }else{
                echo "<p align='center'>No se encontraron resultados para mostrar.</p>";
            }
        }catch(Exception $e){
            echo '<p align="center">'.$e->getMessage().'</p>' ;
        }
  	    ?>       
        </p>
        <p id="logoGobernacion" align='center'>
            <img src="../../img/Logo Gobernacion.gif"/>
        </p>
    </card>
</wml>