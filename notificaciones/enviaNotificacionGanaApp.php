<?php

require_once __DIR__ . '/lib/firebaseLib.php';

//Contantes para conexion a FIREBASE DB
const DEFAULT_URL = 'https://ganapp-28237.firebaseio.com/';
const DEFAULT_TOKEN = 'tQ54fLLwKFWSg4k7T9TxzXOoVU4jTIKGAwW35EXY';
const DEFAULT_PATH_DATASET = '/GANA_DATASETS';
const DEFAULT_PATH_LOG = '/GANA_LOG';
//Constante para End Point metodo dataset_by_term
const URL_DATASETS = 'http://datos.narino.gov.co/?q=api/3/gana/dataset_by_term&id=b3916aab-79a8-4264-af4c-b368c66dca90';
//Constantes para cuerpo y titulo del mensaje
const MENSAJE_CUERPO = 'La Gobernación de Nariño le informa que se han registrado cambios en documentos relacionados con: {t}. Lo invitamos a visitar GANA Datos en GANA App';
const MENSAJE_TITULO = 'Información GANA App!!';
//Contantes para conexion a ONE SIGNAL
const APP_ID_OS = '1f0a301f-35e0-4899-8236-b9cf50d0f160';
const URL_OS = 'https://onesignal.com/api/v1/notifications';
const REST_API_KEY_OS = 'NDZlMzgxODUtM2RmZi00ZTUxLWIwZGItN2VkMmUwNzQ5NGY4';

$vDataSetsActualizados = array();
$vDataSetsRegistrados = array();

//$rand = rand( 1, 300 );

//ini_set('max_execution_time', $rand+10);
date_default_timezone_set('America/Bogota');
//dispara la ejecucion del envio de notificaciones
enviarNotificacion();
//delayCommand('enviarNotificacion', $rand);


/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza el envio de notificaciones a GANA APP modulo de GANA DATOS
 */
function enviarNotificacion()
{
    try
    {
        global $vDataSetsActualizados,$vDataSetsRegistrados;
        //conexion a base de datos
        $vFbCon= conexionDB();        
        //obtengo los datasets de GANA DATOS
        $vDataSets = json_decode(obtenerDataSets(),TRUE);
        //evaluo si es la primera ejecucion del servicio (primer momento)
        $vPrimerMomento= evaluarPrimerMomento($vFbCon);
        try
        {
            //obtiene el mensaje que se enviar a los dispositivos moviles
            $vMensaje = consolidarDatos($vDataSets, $vPrimerMomento, $vFbCon);
            if(!$vPrimerMomento and $vMensaje != null)
            {
                //se envia la notificacion
                $vRespm = notificarCambios($vMensaje);
                //se registra el log del mensaje enviado
                registrarLog($vMensaje, NULL , $vFbCon);
            }
        } 
        catch (Exception $vExProceso) 
        {
            //se deja la revision de los DataSets como estaban originalmente
            if(!$vPrimerMomento)
            {
                foreach($vDataSetsActualizados as $vDsAct)
                {
                    actualizarDataSet($vDsAct, $vFbCon);
                }
                foreach($vDataSetsRegistrados as $vDsReg)
                {
                    $vFbCon->delete(DEFAULT_PATH_DATASET. '/' . $vDsReg['uuid']);
                }
                throw ($vExProceso);
            }
            
        }
        
    } 
    catch (Exception $vEx) 
    {
        registrarLog(NULL, $vEx->getMessage() , $vFbCon);
    }
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza una conexión a la base de datos Firebase
 * @return \Firebase\FirebaseLib
 * @throws Exception
 */
function conexionDB()
{
   try
   {
       return new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
   } 
   catch (Exception $vEx) 
   {
       throw ($vEx);
   } 
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Funcion que realiza la consulta de todos los dataSets de GANADATOS
 * @return boolean
 * @throws Exception
 */
function obtenerDataSets()
{
    try 
    {
        $vCh = curl_init();
        curl_setopt($vCh, CURLOPT_URL, URL_DATASETS);
        curl_setopt($vCh, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($vCh, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($vCh, CURLOPT_HEADER, FALSE);
        curl_setopt($vCh, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($vCh, CURLOPT_SSL_VERIFYPEER, FALSE);
        $vResponse = curl_exec($vCh);
        curl_close($vCh);
        if(!$vResponse) 
        {
            throw new Exception('Error en función obtenerDataSets(), no se obtuvo respuesta en el metodo: '.URL_DATASETS );
        }
        else
        {
            //return $vResponse;
            return '{"help":"Return a a list of datasets :param id: the id or name of the term :type id: string","success":true,"result":[{"name":"Bienes de Inter\u00e9s Cultural","uuid":"7417d578-325d-4c9c-a04f-27b2036668b6","id":"38","revision":"1.4","tema":"Cultura"},{"name":"Directorio de Cultura","uuid":"3b2e2914-ebf1-487f-9849-1a2cdfe55104","id":"21","revision":"1.0","tema":"Agricultura"},{"name":"Directorio de AgriCultura","uuid":"6b2e2914-ebf1-487f-9849-1a2ddfe20104","id":"21","revision":"1.0","tema":"Agricultura"},{"name":"Directorio de Hacienda","uuid":"2b2e2914-ebf1-487f-9849-1a2cdfe20144","id":"21","revision":"1.0","tema":"Hacienda"}]}';
        }
    } 
    catch (Exception $vEx) 
    {
        throw ($vEx);
    }
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Funcion que realiza la validacion de primer momento de ejecucion del servicio
 * @param type $pFireBaseCon
 * @return boolean
 * @throws Exception
 */
function evaluarPrimerMomento($pFireBaseCon)
{
    try 
    {
        $vDs= $pFireBaseCon->get(DEFAULT_PATH_DATASET);
        if(is_null($vDs) or $vDs == 'null')
        {
            return true;
        }
        else
        {
            return false;
        }
    } 
    catch (Exception $vEx) 
    {
        throw ($vEx);
    }
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza el cruce de información para realizar la consolidacion del mensaje final a enviar
 * @param type $pDataSets
 * @param type $pPrimerMomento
 * @param type $pFireBaseCon
 * @return string
 * @throws Exception
 */
function consolidarDatos($pDataSets,$pPrimerMomento,$pFireBaseCon)
{
    try
    {
        global $vDataSetsActualizados,$vDataSetsRegistrados;
        $vTemas = array();
        foreach($pDataSets['result'] as $vDs)
        {
            //se valida si el DataSet existe en la BD
            $vDataSet = $pFireBaseCon->get(DEFAULT_PATH_DATASET . '/' . trim($vDs['uuid']));
            if(is_null($vDataSet) or $vDataSet == 'null')
            {
                //se registra en BD
                array_push($vDataSetsRegistrados,$vDs);
                insertarDataSet($vDs,$pFireBaseCon);
                if(!$pPrimerMomento)
                {
                    array_push($vTemas,strtolower($vDs['tema']) );
                }
            }
            else
            {
                $vDataSetAr = json_decode($vDataSet,true);
                //se verifica si la revision cambio
                if(trim(strtoupper($vDs['revision'])) != trim(strtoupper($vDataSetAr['REVISION'])) )
                {
                    //se actualiza la revision del dataSet
                    $vDst = $vDs;
                    $vDst['revision']= trim(strtoupper($vDataSetAr['REVISION']));
                    array_push($vDataSetsActualizados,$vDst);
                    actualizarDataSet($vDs,$pFireBaseCon);
                    array_push($vTemas,strtolower(trim($vDs['tema'])));
                }
            }
        }
        //se arma el mensaje
        $vMensaje = construirMensaje($vTemas);
        return $vMensaje;
    }
    catch (Exception $vEx) 
    {
        throw ($vEx);
    }
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza la construccion del mensaje a enviar mediante Notificación Push
 * @param $pTemas
 * @return string
 * @throws Exception
 */
function construirMensaje($pTemas)
{
    try
    {
        $vMensaje = NULL;
        $vTemas = array_unique($pTemas);
        foreach($vTemas as $vTema)
        {
            $vMensaje .= ucfirst($vTema) .', ';
        }
        if($vMensaje != NULL)
        {
            $vMensaje= substr($vMensaje, 0, -2);
            $vMensaje= str_replace("{t}", $vMensaje, MENSAJE_CUERPO);
        }
        return $vMensaje;
    } 
    catch (Exception $vEx) 
    {
        throw ($vEx);
    } 
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza el envio de la notificacion Push haciendo uso del API de OneSignal
 * @param type $pMensaje
 * @return type
 * @throws type
 * @throws Exception
 */
function notificarCambios($pMensaje)
{
    try
    {
        $vContenido = array("en" => $pMensaje,
                            "es" => $pMensaje);
        
        $vTitulo = array("en" => MENSAJE_TITULO,
                         "es" => MENSAJE_TITULO);
    
        $vParametros = array('app_id' => APP_ID_OS,
                            'included_segments' => array('All'),
                            'data' => null,
                            'contents' => $vContenido,
                            'headings' => $vTitulo);
		
        $vParametros = json_encode($vParametros);
        $vCh = curl_init();
        curl_setopt($vCh, CURLOPT_URL, URL_OS);
        curl_setopt($vCh, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic '. REST_API_KEY_OS));
        curl_setopt($vCh, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($vCh, CURLOPT_HEADER, FALSE);
        curl_setopt($vCh, CURLOPT_POST, TRUE);
        curl_setopt($vCh, CURLOPT_POSTFIELDS, $vParametros);
        curl_setopt($vCh, CURLOPT_SSL_VERIFYPEER, FALSE);
   
        $vResponse = curl_exec($vCh);
        curl_close($vCh);
        $vResponseArr = json_decode($vResponse,TRUE);
        if(!$vResponse || array_key_exists('errors',$vResponseArr)) 
        {
            throw new Exception('Error en función notificarMensaje(), no se obtuvo respuesta en el metodo: '.URL_OS );
        }
        else
        {
            return $vResponse;
        }
    } 
    catch (Exception $vEx) 
    {
        throw($vEx);
    }
    
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza el registro del Log de envio de mensaje en FireBase
 * @param type $pMensaje
 * @param type $pError
 * @param type $pFireBaseCon
 * @throws Exception
 */
function registrarLog($pMensaje, $pError, $pFireBaseCon)
{
    try
    {
        $vEstado = 'C';
        $vFecha = new DateTime();
        if($pError != NULL)
        {
            $vEstado = 'F';
        }
        //se registra en BD
        $vId = md5(uniqid(null,true));
        $vLogArr = array("FECHA" => $vFecha->format('d/m/Y H:i:s'),
                        "ESTADO" => $vEstado,
                        "ERROR" => (string) $pError,
                        "MENSAJE" => (string) $pMensaje);
        $pFireBaseCon->set(DEFAULT_PATH_LOG ."/". $vId , $vLogArr);
    } 
    catch (Exception $vEx) 
    {
        throw($vEx);
    }
}
    
/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza el registro de un DataSet en FireBase
 * @param type $pDataset
 * @param type $pFireBaseCon
 * @throws Exception
 */
function insertarDataSet($pDataset,$pFireBaseCon)
{
    try
    {
        //se registra en BD
        $vDataSetArr = array("ID" => $pDataset['id'],
                            "REVISION" => trim($pDataset['revision']));
        $pFireBaseCon->set(DEFAULT_PATH_DATASET . '/' . trim($pDataset['uuid']), $vDataSetArr);
    }
    catch (Exception $vEx) 
    {
        throw($vEx);
    }
}

/**
 * @author Andrés Calvachi D.  16/02/2017
 * @uses Función que realiza la actualizaion de la REVISION de un DataSet en FireBase
 * @param type $pDataset
 * @param type $pFireBaseCon
 * @throws Exception
 */
function actualizarDataSet($pDataset,$pFireBaseCon)
{
    try
    {
        //se actualiza la revision del dataSet
        $pFireBaseCon->set(DEFAULT_PATH_DATASET . '/' . trim($pDataset['uuid']) . '/REVISION', trim($pDataset['revision']));
    } 
    catch (Exception $vEx) 
    {
        throw($vEx);
    }
}

/*function delayCommand($callback, $delayTime) {
    sleep($delayTime);
    $callback();
}*/
